//
//  Item.h
//  Xork
//
//  Created by Pietro Rea on 8/8/13.
//  Copyright (c) 2013 Pietro Rea. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JavaScriptCore/JavaScriptCore.h>

@protocol ItemExports <JSExport>

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* description;

@end

@interface Item : NSObject <ItemExports>

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* description;

@end
