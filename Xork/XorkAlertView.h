//
//  XorkAlertView.h
//  Xork
//
//  Created by Pietro Rea on 8/8/13.
//  Copyright (c) 2013 Pietro Rea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JavaScriptCore/JavaScriptCore.h>

@interface XorkAlertView : UIAlertView

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
     successHandler:(JSValue *)successHandler
     failureHandler:(JSValue *)failureHandler
            context:(JSContext *)context;   ;

@end
