//
//  ViewController.m
//  Xork
//
//  Created by Pietro Rea on 8/4/13.
//  Copyright (c) 2013 Pietro Rea. All rights reserved.
//

#import "XorkViewController.h"
#import "ConsoleTextView.h"
#import "XorkAlertView.h"
#import "Item.h"
#import <JavaScriptCore/JavaScriptCore.h>

@interface XorkViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet ConsoleTextView *outputTextView;
@property (strong, nonatomic) IBOutlet UITextField *inputTextField;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;

@property (strong, nonatomic) JSContext *context;
@property (strong, nonatomic) JSManagedValue *inventory;

@end

@implementation XorkViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.inputTextField becomeFirstResponder];
    
    UIFont *navBarFont = [UIFont fontWithName:@"Courier" size:23];
    NSDictionary *attributes = @{NSFontAttributeName : navBarFont};
    [self.navigationBar setTitleTextAttributes:attributes];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.inputTextField.delegate = self;
    [self loadXorkGame];
}

- (void)loadXorkGame {
    NSString *scriptPath = [[NSBundle mainBundle] pathForResource:@"xork" ofType:@"js"];
    NSString *scriptString = [NSString stringWithContentsOfFile:scriptPath encoding:NSUTF8StringEncoding error:nil];
    
    NSString *dataPath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"json"];
    NSString *dataString = [NSString stringWithContentsOfFile:dataPath encoding:NSUTF8StringEncoding error:nil];
    NSData *jsonData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData
                                                         options:0
                                                           error:&error];
    
    if (error) {
        NSLog(@"%@", @"NSJSONSerialization");
        return;
    }
    
    self.context = [[JSContext alloc] init];
    [self.context evaluateScript:scriptString];
    
    __weak XorkViewController *weakSelf = self;
    
    JSValue *value = self.context[@"inventory"];
    self.inventory = [JSManagedValue managedValueWithValue:value];
    [self.context.virtualMachine addManagedReference:self.inventory
                                           withOwner:self];
    
    self.context[@"print"] = ^(NSString* text) {
        text = [NSString stringWithFormat:@"%@\n", text];
        [weakSelf.outputTextView setText:text concatenate:YES];
    };
    
    self.context[@"presentNativeAlert"] = ^(NSString* title,
                                            NSString* message,
                                            JSValue *successHandler,
                                            JSValue *failureHandler) {
        
        JSContext *context = [JSContext currentContext];
        XorkAlertView* alertView = [[XorkAlertView alloc] initWithTitle:title
                                                                message:message
                                                         successHandler:successHandler
                                                         failureHandler:failureHandler
                                                                context:context];
        [alertView show];
    };
    
    self.context[@"getVersion"] = ^{
        NSString* versionString = [[NSBundle mainBundle]
                                   objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        
        versionString = [@"Xork version " stringByAppendingString:versionString];
        
        JSContext *context = [JSContext currentContext];
        JSValue *version = [JSValue valueWithObject:versionString
                                          inContext:context];
        return version;
    };
    
    JSValue *function = self.context[@"startGame"];
    JSValue *dataValue = [JSValue valueWithObject:jsonArray inContext:self.context];
    [function callWithArguments:@[dataValue]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFielDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSString* inputString = textField.text;
    [inputString lowercaseString];
    
    if ([inputString isEqualToString:@"clear"]) {
        [self clearConsole];
    }
    else if ([inputString isEqualToString:@"cheat"]) {
        [self addPantryKeyToInventory];
    }
    else {
        [self processUserInput:inputString];
    }

    [self clearInputTextField];
    
    return YES;
}


#pragma mark - Helper methods

- (void)clearConsole {
    self.outputTextView.text = @"";
}

- (void)clearInputTextField {
    self.inputTextField.text = @"";
}

- (void)addPantryKeyToInventory {
    Item* sword = [[Item alloc] init];
    sword.name = @"pantry key";
    sword.description = @"It looks like a normal key.";
    
    JSValue *inventory = [self.inventory value];
    JSValue *function = inventory[@"addItem"];
    [function callWithArguments:@[sword]];
}

- (void)processUserInput:(NSString *)input {
    
    JSValue *function = self.context[@"processUserInput"];
    JSValue *inputValue = [JSValue valueWithObject:input inContext:self.context];
    [function callWithArguments:@[inputValue]];
}

- (void)saveGame {
    JSValue* function = self.context[@"save"];
    [function callWithArguments:@[]];
}

// 4. Managed reference (pass in alert view in function)
// 5. Better story

//retain cycle: inserting a jsvalue property to a jscontext that is also a property
//VC -> JSVaue
//VC -> JSContext
//JSContext -> JSValue
//Insert user preferences dictionary

//Or, create JS function that takes a button/control.
//The button/control has a public property that is going to be set.

//JSValues take care of their own memory management
//Problems occur when JSValue is stored into a native Objective-C object.

@end
