//
//  XorkAlertView.m
//  Xork
//
//  Created by Pietro Rea on 8/8/13.
//  Copyright (c) 2013 Pietro Rea. All rights reserved.
//

#import "XorkAlertView.h"

@interface XorkAlertView() <UIAlertViewDelegate>

@property (strong, nonatomic) JSContext *ctxt;
@property (strong, nonatomic) JSManagedValue *successHandler;
@property (strong, nonatomic) JSManagedValue *failureHandler;

@end

@implementation XorkAlertView

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
     successHandler:(JSValue *)successHandler
     failureHandler:(JSValue *)failureHandler
            context:(JSContext *)context {
    
    self = [super initWithTitle:title
                        message:message
                       delegate:self
              cancelButtonTitle:@"No"
              otherButtonTitles:@"Yes", nil];
    
    if (self) {
        // Initialization code
        _ctxt = context;
        
        _successHandler = [JSManagedValue managedValueWithValue:successHandler];
        [context.virtualMachine addManagedReference:_successHandler withOwner:self];
        
        _failureHandler = [JSManagedValue managedValueWithValue:successHandler];
        [context.virtualMachine addManagedReference:_failureHandler withOwner:self];
    }
    return self;
}

- (void)dealloc {
    
    //TODO: is this the right place to remove managed reference?
    [self.ctxt.virtualMachine removeManagedReference:_successHandler withOwner:self];
    [self.ctxt.virtualMachine removeManagedReference:_successHandler withOwner:self];
}

#pragma mark - UIAlertviewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == self.cancelButtonIndex) {
        JSValue *function = [self.failureHandler value];
        [function callWithArguments:@[]];
    }
    else {
        JSValue *function = [self.successHandler value];
        [function callWithArguments:@[]];
    }
}

@end
